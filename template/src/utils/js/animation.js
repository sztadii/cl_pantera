var animation;

animation = new function () {

  //private vars
  var breakpoint = 1024;

  //public functions
  this.fading = function ($el) {
    var object = $($el);

    if (object.length && window.innerWidth >= breakpoint) {
      var wScroll = $(window).scrollTop();
      var value = 100 / (wScroll + 20);

      object.css({
        'opacity': value < 1 ? value : 1
      });
    } else {
      object.css({
        'opacity': '1'
      });
    }
  };
};