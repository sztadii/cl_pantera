var fullWindow;

fullWindow = new function () {

  //private vars
  var breakpoint = 1024;

  //catch DOM
  var $height;
  var $width;
  var $objects;

  //bind events
  $(document).ready(function () {
    init();
  });

  $(window).bind('resize orientationchange', function () {

    if ($(window).width() != $width) {
      changeHeight();
    }
  });

  //private functions
  var init = function () {
    $objects = $('.fullWindow');

    changeHeight();
  };

  var changeHeight = function () {
    $height = $(window).height();
    $width = $(window).width();

    $objects.css({'height': $height});
  };

  var resetHeight = function () {
    $objects.removeAttr('style');
  };
};