var form;

form = new function () {

  //catch DOM
  var $el;
  var $form;
  var $textarea;
  var $inputs;

  //bind events
  $(document).ready(function () {
    init()
  });

  //public functions
  var init = function () {
    $el = $('.form');
    $form = $el.find('.form__box');
    $textarea = $el.find('.form__textarea');
    $inputs = $el.find('.form__input');

    watchForm();
    submitForm();
  };

  //private functions
  var watchForm = function () {
    $textarea.on("focus", function () {
      $(this).addClass("-visited")
    });

    $inputs.bind('change blur', (function () {
      if ($(this).is(":valid")) {
        $(this).removeClass("-invalid")
      } else {
        $(this).addClass("-invalid")
      }
    }));

    $inputs.focus(function () {
      $(this).removeClass("-invalid");
    });

    $inputs.on("invalid", function () {
      $(window).width() <= 1023 && $inputs.trigger("blur")
    })
  };

  var submitForm = function () {
    var submitPause;

    $form.submit(function (event) {
      event.preventDefault();
      clearTimeout(submitPause);

      $.ajax({
        type: 'POST',
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function () {
          $('.form__msg.-positive').removeClass('-hide');
          $form.trigger('reset');
        },
        error: function () {
          $('.form__msg.-negative').removeClass('-hide');
        },
        complete: function () {
          submitPause = setTimeout(function () {
            $('.form__msg').addClass('-hide');
          }, 2000);
        }
      });
    });
  };
};