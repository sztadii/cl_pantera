var banner;

banner = new function () {

  //catch DOM
  var $el;
  var $sliderTiny;
  var $sliderBig;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $('.banner');
    $sliderTiny = $el.find('.banner__tiny');
    $sliderBig = $el.find('.banner__big');

    sliderTinyMake();
    sliderBigMake();
  };

  var sliderTinyMake = function () {

    if ($sliderTiny.length > 0) {
      $sliderTiny.imagesLoaded({background: true}).always(function () {
        $sliderTiny.slick({
          asNavFor: $sliderBig,
          infinite: true,
          dots: false,
          arrows: true,
          appendArrows: $('.banner__tinyArrows .container'),
          prevArrow: '<div class="banner__tinyArrow -prev"><i class="fa fa-angle-left"></i><span class="banner__tinyArrowText">poprzedni banner</span></div>',
          nextArrow: '<div class="banner__tinyArrow -next"><span class="banner__tinyArrowText">następny banner</span><i class="fa fa-angle-right"></i></div>',
          slidesToShow: 1,
          slidesToScroll: 1,
          fade: true,
          adaptiveHeight: true,
          autoplay: true,
          speed: 800,
          autoplaySpeed: 4000,
          draggable: false,
          pauseOnHover: false,
          pauseOnFocus: false
        });
      });
    }
  };

  var sliderBigMake = function () {

    if ($sliderBig.length > 0) {
      $sliderBig.imagesLoaded({background: true}).always(function () {
        $sliderBig.slick({
          asNavFor: $sliderTiny,
          infinite: true,
          dots: false,
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          fade: true,
          adaptiveHeight: true,
          autoplay: true,
          speed: 800,
          autoplaySpeed: 4000,
          draggable: false,
          pauseOnHover: false,
          pauseOnFocus: false
        });
      });
    }
  };
};
