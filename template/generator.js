var generator;

generator = new function () {

  //private vars
  var fs = require('fs');
  var args = process.argv.slice(2);
  var dir = './src/components/';
  var filesTypes = ['.pug', '.sass', '.js'];

  //public functions
  this.init = function () {
    if (args.length) {
      start();
    } else {
      msg('Forgot args.');
    }
  };

  //private functions
  var start = function () {
    if (args[0] == 'add') {
      for (var i = 1; i < args.length; i++) {
        componentAdd(args[i]);
      }
    } else if (args[0] == 'remove') {
      for (var i = 1; i < args.length; i++) {
        componentRemove(args[i]);
      }
    } else {
      msg('Not found command.');
    }
  };

  var msg = function (msg) {
    console.log(msg);
  };

  var componentAdd = function (name) {
    var path = dir + name + '/';

    if (!fs.existsSync(path)) {
      dirAdd(path);

      for (var i = 0; i < filesTypes.length; i++) {
        fileAdd(path + name + filesTypes[i]);
      }

      msg('You make ' + name + ' component.');
    } else {
      msg('Component exits.');
    }
  };

  var componentRemove = function (name) {
    var path = dir + name + '/';

    if (fs.existsSync(path)) {
      for (var i = 0; i < filesTypes.length; i++) {
        fileRemove(path + name + filesTypes[i]);
      }

      dirRemove(path);

      msg('You remove ' + name + ' component.');
    } else {
      msg('Component ' + name + ' do not exits.');
    }
  };

  var dirAdd = function (path) {
    fs.mkdirSync(path);
  };

  var dirRemove = function (path) {
    fs.rmdirSync(path);
  };

  var fileAdd = function (filePath) {
    fs.open(filePath, "wx", function (err, fd) {
      fs.close(fd, function (err) {
      });
    });
  };

  var fileRemove = function (filePath) {
    fs.unlinkSync(filePath);
  };
};

generator.init();

//HOW USE
//node generator add banner gallery nav
//node generator remove banner nav
